//
//  NoMarkdownTestClassFound.m
//  mdtkit-apple
//
//  Created by Florian Obermayer on 25/07/16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MDTKit.h"
#import "MDParserException.h"

@interface NoMarkdownTestClassFound : MDTestCase

@end

@implementation NoMarkdownTestClassFound


- (void)testNoMarkdownTestClassFoundTest
{
    
    [super AssertExpectExceptionOfType: [MDParserException class] withMessageStartingWith:@"No file found: 'MyNotInMarkdownTestFolderFindableClass.md'" inBlock: ^{
        [super runOnEachRowWithTestName:@"NoMarkdownTestClassFoundTest" testFileName:@"MyNotInMarkdownTestFolderFindableClass" withBlock:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}];
    }];
}
@end
