//
//  MultipleMarkdownTestMethodsFound.m
//  mdtkit-apple
//
//  Created by Florian Obermayer on 25/07/16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MDLinterException.h"
#import "MDTKit.h"

@interface MultipleMarkdownTestMethodsFound : MDTestCase

@end

@implementation MultipleMarkdownTestMethodsFound


- (void)testMultipleMarkdownTestMethodsFoundTest
{
//ExpectExceptionInAggregateExceptionOrDirectly
    [super AssertExpectExceptionOfType: [MDLinterException class] withMessageStartingWith:@"Duplicate declaration of TestCase 'MultipleMarkdownTestMethodsFoundTest'" inBlock: ^{
        [super runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}];
    }];
}
@end
