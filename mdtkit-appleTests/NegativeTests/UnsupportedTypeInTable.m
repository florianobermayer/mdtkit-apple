//
//  UnsupportedTypeInTable.m
//  mdtkit-apple
//
//  Created by Florian Obermayer on 25/07/16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MDLinterException.h"
#import "MDTKit.h"

@interface UnsupportedTypeInTable : MDTestCase

@end

@implementation UnsupportedTypeInTable


- (void)testUnsupportedTypeInTableTest
{
    //ExpectExceptionInAggregateExceptionOrDirectly
    [super AssertExpectExceptionOfType: [MDLinterException class] withMessageStartingWith:@"Unsupported type: 'unsupported_type'" inBlock: ^{
        [super runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}];
    }];
}
@end
