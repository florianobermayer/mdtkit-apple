//
//  NoTable.m
//  mdtkit-apple
//
//  Created by Florian Obermayer on 25/07/16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MDLinterException.h"
#import "MDTKit.h"

@interface NoTable : MDTestCase

@end

@implementation NoTable


- (void)testNoTableTest
{
    [super AssertExpectExceptionOfType: [MDLinterException class] withMessageStartingWith:@"No table defined for test: 'NoTableTest'" inBlock: ^{
        [super runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}];
    }];
}
@end
