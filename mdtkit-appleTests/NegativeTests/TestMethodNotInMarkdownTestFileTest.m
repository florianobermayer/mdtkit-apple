//
//  TestMethodNotInMarkdownTestFileTest.m
//  mdtkit-apple
//
//  Created by Florian Obermayer on 25/07/16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MDTKit.h"
#import "MDParserException.h"

@interface TestMethodNotInMarkdownTestFileTest : MDTestCase

@end

@implementation TestMethodNotInMarkdownTestFileTest


- (void)testThisTestMethodIsNotFoundInTheTestFile
{
    [super AssertExpectExceptionOfType: [MDParserException class] withMessageStartingWith:@"Cannot find 'testThisTestMethodIsNotFoundInTheTestFile' in file 'TestMethodNotInMarkdownTestFileTest.md'!" inBlock: ^{
        [super runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}];
    }];
}
@end
