//
//  NotProperlyFormattedStringInTable.m
//  mdtkit-apple
//
//  Created by Florian Obermayer on 25/07/16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MDLinterException.h"
#import "MDTKit.h"

@interface NotProperlyFormattedStringInTable : MDTestCase

@end

@implementation NotProperlyFormattedStringInTable


- (void)testNotProperlyFormattedStringInTable
{
    //XCTAssertThrowsSpecificNamed([super runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}], MDLinterException, @"Duplicate header: '");
    [super AssertExpectExceptionOfType: [MDLinterException class] withMessageStartingWith:@"Could not resolve arithmetic expression: 'I am mal-formatted because I'm not surrounded by matching quotes':\nSyntaxError: Unexpected identifier" inBlock: ^{
        [super runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}];
    }];
}
@end
