//
//  DoublePreConstantAssignments.m
//  mdtkit-apple
//
//  Created by Florian Obermayer on 25/07/16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MDLinterException.h"
#import "MDTKit.h"

@interface DoublePreConstantAssignments : MDTestCase

@end

@implementation DoublePreConstantAssignments


- (void)testDoublePreConstantAssignments
{
    [super AssertExpectExceptionOfType: [MDLinterException class] withMessageStartingWith:@"Duplicate declaration of pre-constant 'double_assigned_var'" inBlock: ^{
        [super runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}];
    }];
}
@end
