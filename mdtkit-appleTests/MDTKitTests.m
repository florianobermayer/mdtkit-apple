//
//  MDTKitTests.m
//  MDTKit
//
//  Created by Florian Obermayer 2016/06/23.
//  Copyright (c) 2016 Secomba GmbH. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MDParserException.h"
#import "MDTKit.h"

@interface MyNotInMarkdownTestFolderFindableClass : NSData

@end

@implementation MyNotInMarkdownTestFolderFindableClass

@end

@interface MDTKitTests : MDTestCase

@end

@implementation MDTKitTests

-(void)testVerboseSmoke
{
    __block NSMutableDictionary *preConst;
    __block NSMutableDictionary *postConst;
    __block NSMutableDictionary *preVar;
    __block NSMutableDictionary *postVar;
    __block NSMutableDictionary *preLinkedConst;
    __block NSMutableDictionary *postLinkedConst;
    
    
    [super dataForTest:@"VerboseSmokeTest" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {
        [self runVerboseWithRow:row index:index pre:pre post:post];
        
        if (!preConst || !postConst || !preVar || !postVar || !preLinkedConst || !postLinkedConst) {
            
            preConst = [[NSMutableDictionary alloc]init];
            postConst = [[NSMutableDictionary alloc]init];
            preVar = [[NSMutableDictionary alloc]init];
            postVar = [[NSMutableDictionary alloc]init];
            preLinkedConst = [[NSMutableDictionary alloc]init];
            postLinkedConst = [[NSMutableDictionary alloc]init];           
            
            for (NSString* key in pre) {
                if ([key hasPrefix:@"c_"]) {
                    [preConst setObject:pre[key] forKey:[key stringByReplacingOccurrencesOfString:@"c_" withString:@""]];
                }
            }
            
            for (NSString* key in post) {
                if ([key hasPrefix:@"c_"]) {
                    [postConst setObject:pre[key] forKey:[key stringByReplacingOccurrencesOfString:@"c_" withString:@""]];
                }
            }
            
            for (NSString* key in pre) {
                if ([key hasPrefix:@"cl_"]) {
                    [preLinkedConst setObject:pre[key] forKey:[key stringByReplacingOccurrencesOfString:@"cl_" withString:@""]];
                }
            }
            
            for (NSString* key in post) {
                if ([key hasPrefix:@"cl_"]) {
                    [postLinkedConst setObject:pre[key] forKey:[key stringByReplacingOccurrencesOfString:@"cl_" withString:@""]];
                }
            }
            
            for (NSString* key in pre) {
                if (![key hasPrefix:@"c_"] && ![key hasPrefix:@"cl_"]) {
                    [preVar setObject:pre[key] forKey:key];
                }
            }
            
            for (NSString* key in post) {
                if (![key hasPrefix:@"c_"] && ![key hasPrefix:@"cl_"]) {
                    [postVar setObject:post[key] forKey:key];
                }
            }
            
            for (NSString *prePairKey in preConst) {
                XCTAssertNotEqualObjects(prePairKey, preConst[prePairKey]);
            }
            
            for (NSString *prePairKey in preLinkedConst) {
                XCTAssertNotEqualObjects(prePairKey, preLinkedConst[prePairKey]);
            }
            
            // test for negative equality
            XCTAssertEqual(@([preVar[@"int_nr"] integerValue]), @(-[postVar[@"int_nr"] integerValue]));
            XCTAssertEqualWithAccuracy([preVar[@"double_nr"] doubleValue], -[postVar[@"double_nr"] doubleValue],0.0001);
            XCTAssertEqual(@([preVar[@"bool"] integerValue]), @([postVar[@"bool"] integerValue] -1));
            
            // test for equlity
            XCTAssertEqual(preVar[@"date"], postVar[@"date"]);
            XCTAssertEqualObjects(preVar[@"string"], postVar[@"string"]);
            XCTAssertEqualObjects(preVar[@"string_ml"], postVar[@"string_ml"]);
            
            XCTAssertEqual(preVar[@"undefined_string"], postVar[@"undefined_string"]);

            if (![preVar[@"undefined_string"] isEqual:[NSNull null]]) {
                XCTFail(@"undefined_string is not NSNull");
            }
            
            if (![postVar[@"undefined_string"] isEqual:[NSNull null]]) {
                XCTFail(@"undefined_string is not NSNull");
            }
        }
    }];
}

-(void)runVerboseWithRow:(NSDictionary *)row index:(NSInteger)index pre:(NSDictionary *)pre post:(NSDictionary *)post
{
    NSString *undefinedTypeName = @"undefined";
    int minHeadingTypeSize = [undefinedTypeName length];
    
    NSMutableArray *headings = [[NSMutableArray alloc] init];
    
    for (NSString *key in row) {
        NSString *typeName = [[row[key] class] description];
        if([row[key] isKindOfClass: [NSString class]]){
            typeName = @"string";
        }else if ([row[key] isKindOfClass: [NSDate class]]){
            typeName = @"date";
        }else if ([[[row[key] class] description] isEqualToString:@"__NSCFBoolean"]){
            typeName = @"boolean";
        }else if ([row[key] isKindOfClass: [NSNumber class]]){
            typeName = @"number";
        }else {
            typeName = undefinedTypeName;
        }
        
        while([typeName length] < minHeadingTypeSize){
            typeName = [typeName stringByAppendingString:@" "];
        }
        
        NSMutableString *hfmt = [[NSMutableString alloc] initWithString:@" %1$@:%2$@ "];
        
        [headings addObject: [NSString stringWithFormat:hfmt, key, typeName]];
    }
    
    if (index == 0) {
        int minEntrySize = 20;
        NSString * fmt = @"   %1$@ %2$@";
        NSLog(@"Pre-Constants:");
        for (NSString* entry in pre) {
            NSString *tmpEntry = [NSString stringWithString:entry];
            while([tmpEntry length] < minEntrySize){
                tmpEntry = [tmpEntry stringByAppendingString:@" "];
            }
            NSLog(fmt, tmpEntry, [pre objectForKey:entry]);
        }
        NSLog(@"%@", @"\n");
        NSLog(@"Post-Constants:");
        for (NSString* entry in post) {
            NSString *tmpEntry = [NSString stringWithString:entry];
            while([tmpEntry length] < minEntrySize){
                tmpEntry = [tmpEntry stringByAppendingString:@" "];
            }
            NSLog(fmt, tmpEntry, [post objectForKey:entry]);
        }
        NSLog(@"%@", @"\n");
        
        NSLog(@"Table:");
        NSMutableString *headerLine = [[NSMutableString alloc] initWithString:@"   |"];
        for (int i = 0; i < [headings count]; i++) {
            [headerLine appendFormat: @"%@|", headings[i]];
        }
        
        NSLog(@"%@", headerLine);
        NSString *secondLine = @"   |";
 
        secondLine = [secondLine stringByPaddingToLength:([headerLine length] -1) withString: @"-" startingAtIndex:0];
        secondLine = [secondLine stringByAppendingString:@"|"];
        
        NSLog(@"%@", secondLine);
    }
    
    NSMutableArray *rowValues =  [[NSMutableArray alloc] init];
    for (NSString *key in row) {
        NSString *val = [NSString stringWithFormat:@"%@", row[key]];
        if(val){
            val = [val stringByReplacingOccurrencesOfString:@"\t" withString:@"\\t"];
            val = [val stringByReplacingOccurrencesOfString:@"\r\n" withString:@"\\n"];
            val = [val stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
            val = [NSString stringWithFormat:@" %@", val];
        }
        [rowValues addObject:val];
    }
    
    NSString *rowLine = @"   |";
    for (int i = 0; i < [headings count]; i++) {
        NSString *tmpVal = [NSString stringWithFormat:@"%@", rowValues[i]];
        while ([tmpVal length ] < [headings[i] length]) {
            tmpVal = [tmpVal stringByAppendingString:@" "];
        }
        rowLine = [rowLine stringByAppendingString:tmpVal];
        rowLine = [rowLine stringByAppendingString:@"|"];
    }

    NSLog(@"%@", rowLine);
}

-(void)testDataTypes
{
    [super dataForTest:@"DataTypes" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {
        [self runVerboseWithRow:row index:index pre:pre post:post];
    }];
}
-(void)testLinkedTables
{
    [super dataForTest:@"LinkedTables" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {
        [self runVerboseWithRow:row index:index pre:pre post:post];
        
        XCTAssertEqual(@([row[@"bool_exp"] integerValue]), @([row[@"bool_val"] integerValue]));
        XCTAssertEqualWithAccuracy([row[@"number_exp"] doubleValue], [row[@"number_val"] doubleValue],0.0001);
    }];
}
/*
-(void)testNoTable
{
    XCTAssertThrowsSpecificNamed([super dataForTest:@"NoTableTest" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}], MDParserException, @"no table found");
}

-(void)testNoMarkdownTestClassFoundTest
{
    
    XCTAssertThrowsSpecificNamed([super dataForTest:@"NoMarkdownTestClassFoundTest" class:[MyNotInMarkdownTestFolderFindableClass class] runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}], MDParserException, @"no file found: 'MyNotInMarkdownTestFolderFindableClass.md'");
}

-(void)testMultipleMarkdownTestMethodsFoundTest
{
    
    XCTAssertThrowsSpecificNamed([super dataForTest:@"MultipleMarkdownTestMethodsFoundTest" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}], MDParserException, @"multiple tests matching: 'MultipleMarkdownTestMethodsFoundTest' found");
}

-(void)testNoMarkdownTestMethodFoundTest
{
    
    XCTAssertThrowsSpecificNamed([super dataForTest:@"NoMarkdownTestMethodFoundTest" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}], MDParserException, @"no test matching: 'NoMarkdownTestMethodFoundTest' found");
}

-(void)testUnsupportedTypeInConstTest
{
    XCTAssertThrowsSpecificNamed([super dataForTest:@"UnsupportedTypeInConstTest" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}], MDParserException, @"unsupported type: 'unsupported_type'");
}

-(void)testUnsupportedTypeInTableTest
{
    XCTAssertThrowsSpecificNamed([super dataForTest:@"UnsupportedTypeInTableTest" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}], MDParserException, @"unsupported type: 'unsupported_type'");
}

-(void)testDoublePreConstantAssignmentsTest
{
    XCTAssertThrowsSpecificNamed([super dataForTest:@"DoublePreConstantAssignmentsTest" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}], MDParserException, @"variable 'double_assigned_var' previously declared");
}

-(void)testDoubleHeadingAssignmentsTest
{
    XCTAssertThrowsSpecificNamed([super dataForTest:@"DoubleHeadingNameTest" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}], MDParserException, @"heading 'double_heading_name' previously declared");
}

-(void)testNotProperlyFormattedStringInTableTest
{
     XCTAssertThrowsSpecificNamed([super dataForTest:@"NotProperlyFormattedStringInTableTest" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}], MDParserException, @"string not properly formatted: [I am mal-formatted because I'm not surrounded by matching quotes]");
}
*/
-(void)testArithmeticExpressions
{
    [super dataForTest:@"ArithmeticExpressions" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {
        [self runVerboseWithRow:row index:index pre:pre post:post];
        XCTAssertEqual(@([row[@"booleanExpression"] integerValue]), @([row[@"booleanResult"] integerValue]));
        XCTAssertEqualWithAccuracy([row[@"numberExpression"] doubleValue], [row[@"numberResult"] doubleValue],0.0001);
    }];
}

-(void)testPathSeparators{
    NSString * separator = @"/";
    [super dataForTest:@"PathSeparators" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {
        [self runVerboseWithRow:row index:index pre:pre post:post];
        XCTAssertEqualObjects(separator, pre[@"separator"]);
    }];
}
/*
-(void)testConstantResolving
{
    [super dataForTest:@"ConstantResolving" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {
        
        for (NSString *preKey in pre) {
            XCTAssertEqual(pre[preKey], post[preKey]);
        }
    }];
}

-(void)testConstantResolvingFail
{
    XCTAssertThrowsSpecificNamed([super dataForTest:@"ConstantResolvingFail" class:@"ConstantResolvingFail" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}], MDParserException, @"Entry with Key:'IIIUNUSED' and Value:'$IIUNUSED' can't be resolved");
}

*/

@end
