//
//  MDTKitConstResolvingTest.m
//  mdtkit-apple
//
//  Created by Florian Obermayer on 25/07/16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MDTestCase.h"
#import "MDLinterException.h"

@interface MDTKitConstResolvingTest : MDTestCase

@end

@implementation MDTKitConstResolvingTest


-(void)testConstantResolving
{
    [super dataForTest:@"ConstantResolving" runOnEachRow:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {
        
        for (NSString *preKey in pre) {
            XCTAssertEqual(pre[preKey], post[preKey]);
        }
    }];
}


-(void)testConstantResolvingFail
{
    [super AssertExpectExceptionOfType: [MDLinterException class] withMessageStartingWith:@"Could not resolve value '$IIIIIUNUSED'" inBlock: ^{
        [super runOnEachRowWithTestName:@"ConstantResolvingFail" testFileName: @"ConstantResolvingFail" withBlock:^(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post) {}];
    }];
}

@end
