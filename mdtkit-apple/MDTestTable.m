//
//  MDTestTable.m
//  MDTKit
//
//  Created by Florian Obermayer 2016/06/23.
//  Copyright (c) 2016 Secomba GmbH. All rights reserved.
//

#import "MDTestTable.h"


@implementation MDTestTable

- (instancetype)initWithHeadings:(NSArray *)headings rows:(NSArray *)rows
{
    self = [super init];
    if(self) {
        _headings = headings;
        _rows = rows;
    }
    return self;
}

- (NSArray *)allRows
{
    return [self rows];
}

@end

