//
//  MDLinterException.h
//  mdtkit-apple
//
//  Created by Florian Obermayer on 23.06.16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDLinterException : NSException

+(void)raiseWithMessage:(NSString*)message location:(NSDictionary *)location path:(NSString*)path ;

@end

@interface MDLinterStackException : NSException

+(void)raiseWithMessage:(NSString*)message location:(NSDictionary *)location path:(NSString*)path ;

@end
