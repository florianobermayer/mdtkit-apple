//
//  MDTestTableTestCase.m
//  MDTKit
//
//  Created by Florian Obermayer 2016/06/23.
//  Copyright (c) 2016 Secomba GmbH. All rights reserved.
//

#import "MDTestTable.h"
#import "MDTestCase.h"
#import "MDTestData.h"

@implementation MDTestCase

- (void) runOnEachRow :(void (^)(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post)) run
{
    // from http://stackoverflow.com/questions/1451342/objective-c-find-caller-of-method
    NSString *sourceString = [[NSThread callStackSymbols] objectAtIndex:1];
    // Example: 1   UIKit                               0x00540c89 -[UIApplication _callInitializationDelegatesForURL:payload:suspended:] + 1163
    NSCharacterSet *separatorSet = [NSCharacterSet characterSetWithCharactersInString:@" -[]+?.,"];
    NSMutableArray *array = [NSMutableArray arrayWithArray:[sourceString  componentsSeparatedByCharactersInSet:separatorSet]];
    
    [array removeObject:@""];
    [self runOnEachRowWithTestName:[array objectAtIndex:5] withBlock:run];
}

- (void)runOnEachRowWithTestName:(NSString *)testName withBlock:(void (^)(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post))run
{
    [self runOnEachRowWithTestName:testName testClass:[self class] withBlock:run];
}

- (void) runOnEachRowWithTestName:(NSString *)testName testFileName:(NSString *)testFileName withBlock:(void (^)(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post)) run
{
    NSError *error;
    MDTestData *testData = [MDTestData testDataWithTestFileName:testFileName testName:testName error:&error];
    MDTestTable *table = [testData testTable];
    NSAssert(testData, @"TestData not found: %@", [error localizedDescription]);
    for(NSInteger rowIndex = 0; rowIndex < [[table allRows] count]; rowIndex++) {
        NSDictionary *row = [table allRows][rowIndex];
        run(row, rowIndex, [testData preConstants], [testData postConstants]);
    }
}

- (void) runOnEachRowWithTestName:(NSString *)testName testClass:(Class)testClass withBlock:(void (^)(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post)) run
{
    NSError *error;
    MDTestData *testData = [MDTestData testDataWithClass:testClass testName:testName error:&error];
    MDTestTable *table = [testData testTable];
    NSAssert(testData, @"TestData not found: %@", [error localizedDescription]);
    for(NSInteger rowIndex = 0; rowIndex < [[table allRows] count]; rowIndex++) {
        NSDictionary *row = [table allRows][rowIndex];
        run(row, rowIndex, [testData preConstants], [testData postConstants]);
    }
}


-(void) dataForTest:(NSString *) testname runOnEachRow:(void (^)(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post)) run
{
    [self runOnEachRowWithTestName:testname withBlock:run];
}

-(void) dataForTest:(NSString *)testName class:(Class)testClass runOnEachRow:(void (^)(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post)) run
{
    [self runOnEachRowWithTestName:testName testClass:testClass withBlock:run];
}

- (NSString *)assertWrongStringForTable:(NSString *)table row:(NSDictionary *)row rowIndex:(NSInteger)rowIndex
{
    NSMutableString *result = [[NSMutableString alloc] init];
    [result appendFormat:@"Row %ld in %@\n", rowIndex, table];
    [result appendFormat:@"%@", [row description]];
    return [result copy];
}

-(void) AssertExpectExceptionOfType:(Class)exceptionClass withMessageStartingWith:(NSString *)messagePrefix inBlock:(void (^)()) run
{
    BOOL thrown = NO;
    @try {
        run();
    } @catch (NSException *exception) {
        XCTAssertTrue([exception isKindOfClass: exceptionClass]);
        XCTAssertTrue([[exception name] hasPrefix: messagePrefix], @"<%@> is not a prefix of <%@>", messagePrefix, [exception name]);
        thrown = YES;
    }
    
    if(!thrown){
        XCTFail(@"Exception of tyoe %@ not thrown!", [exceptionClass description]);
    }
    
}


@end
