//
//  MDLinterException.m
//  mdtkit-apple
//
//  Created by Florian Obermayer on 23.06.16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import "MDLinterException.h"

@implementation MDLinterException

+(void)raiseWithMessage:(NSString*)message location:(NSDictionary *)location path:(NSString*)path
{
    [super raise:[NSString stringWithFormat:@"%@\n\nLocation:\n%@\n\nPath:\n%@", message, location, path] format:@""];
}

@end


@implementation MDLinterStackException

+(void)raiseWithMessage:(NSString*)message location:(NSDictionary *)location path:(NSString*)path
{
    [super raise:[NSString stringWithFormat:@"%@\n\nLocation:\n%@\n\nPath:\n%@", message, location, path] format:@""];
}

-(NSString * ) formatBlockMessage:(NSString *)message
{
    NSRange range = [message rangeOfString:@"_"];
    NSString * format = @"Malformatted area: '%@'";
    if(range.location == NSNotFound)
    {
        return [NSString stringWithFormat:format, @"Unknown area"];
    }
    return [NSString stringWithFormat:format, [message substringFromIndex:range.location]];
    
}
@end

