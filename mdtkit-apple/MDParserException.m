//
//  MDLinterException.m
//  mdtkit-apple
//
//  Created by Florian Obermayer on 23.06.16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import "MDParserException.h"

@implementation MDParserException

+(void)raiseWithMessage:(NSString*)message
{
    [super raise:message format:@""];
}

@end
