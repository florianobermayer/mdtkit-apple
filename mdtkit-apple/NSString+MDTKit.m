//
//  NSString+MDTKit.m
//  MDTKit
//
//  Created by Florian Obermayer 2016/06/23.
//  Copyright (c) 2016 Secomba GmbH. All rights reserved.
//

#import "NSString+MDTKit.h"

@implementation NSString (MDTKit)

- (NSString *)trim
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)removeAllWhitespace
{
    return [self stringByReplacingOccurrencesOfString:@"\\s" withString:@""
                                              options:NSRegularExpressionSearch
                                                range:NSMakeRange(0, [self length])];
}

- (NSString *)removeAllNewLines
{
    NSArray *components = [self componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    return [components componentsJoinedByString:@""];
}

-(NSArray *) componentsBySplittingStringAtFirstOccurrenceOfString:(NSString *)string
{
    NSRange rOriginal = [self rangeOfString: string];
    NSMutableArray *splitResult = [[NSMutableArray alloc] init];
    [splitResult addObject:[self substringToIndex:rOriginal.location]];
    [splitResult addObject:[self substringFromIndex:rOriginal.location+rOriginal.length]];
    return splitResult;
}

- (NSString *)stringByRemovingString:(NSString *)string
{
    return [self stringByReplacingOccurrencesOfString:string withString:@""];
}

@end