//
//  PropertyLoader.m
//  mdtkit-apple
//
//  Created by Florian Obermayer on 23.06.16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import "PropertyLoader.h"
#import "MDParserException.h"

static NSDictionary * _settings;

@implementation PropertyLoader
NSString * const ConfigFileName = @"settings";

NSString * const NODE_PROPERTY_KEY = @"node-exec-path";
NSString * const NPM_PROPERTY_KEY = @"npm-exec-path";
NSString * const MDTKIT_PROPERTY_KEY = @"mdtkit-parser-dir-path";

+(NSString *) NodePath
{
    NSString * result = [[self class] getPropertiesFromFile][NODE_PROPERTY_KEY];
    if(!result)
    {
        [MDParserException raiseWithMessage:[NSString stringWithFormat:@"'%@' not specified properly in %@. Please add the (absolute) exec path of node to %@.", NODE_PROPERTY_KEY, ConfigFileName, ConfigFileName]];
    }
    return result;
}

+(NSString *) NPMPath
{
    NSString * result = [[self class] getPropertiesFromFile][NPM_PROPERTY_KEY];
    if(!result)
    {
        [MDParserException raiseWithMessage:[NSString stringWithFormat:@"'%@' not specified properly in %@. Please add the (absolute) exec path of npm to %@.", NPM_PROPERTY_KEY, ConfigFileName, ConfigFileName]];
    }
    return result;
}

+(NSString *) MDTKitModuleWorkingDir
{
    NSString * result = [[self class] getPropertiesFromFile][MDTKIT_PROPERTY_KEY];
    if(!result)
    {
        [MDParserException raiseWithMessage:[NSString stringWithFormat:@"'%@' not specified properly in %@. Please add the (relative) path to the mdtkit-parser node module directory to %@.", MDTKIT_PROPERTY_KEY, ConfigFileName, ConfigFileName]];
    }
    return result;
}

+(NSDictionary *) getPropertiesFromFile
{
    if(_settings)
    {
        return _settings;
    }
    
    NSString * settingsFileNameWithExt = [[NSString alloc] initWithFormat:@"/%@.plist", ConfigFileName];
    NSString * bundleResourcePath = [[NSBundle bundleForClass:[self class]] resourcePath];
    NSString * path = [bundleResourcePath stringByAppendingPathComponent:settingsFileNameWithExt];
    
    _settings = [[NSDictionary alloc] initWithContentsOfFile:path];
    return _settings;
}

@end
