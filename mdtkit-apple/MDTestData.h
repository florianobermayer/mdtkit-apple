//
//  MDTestData.h
//  MDTKit
//
//  Created by Florian Obermayer 2016/06/23.
//  Copyright (c) 2016 Secomba GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MDTestTable.h"

@interface MDTestData : NSObject

extern NSString * const TestDataFolderName;

@property (strong, nonatomic) MDTestTable *testTable;
@property (copy, nonatomic) NSDictionary *preConstants;
@property (copy, nonatomic) NSDictionary *postConstants;

+ (instancetype)testDataWithClass:(Class)class testName:(NSString *)testName error:(NSError *__autoreleasing *)error;

+ (instancetype)testDataWithTestFileName:(NSString *)testFileName testName:(NSString *)testName error:(NSError *__autoreleasing *)error;


@end
