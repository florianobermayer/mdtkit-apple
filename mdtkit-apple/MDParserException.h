//
//  MDParserException.h
//  MDTKit
//
//  Created by Florian Obermayer 2016/06/23.
//  Copyright (c) 2016 Secomba GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDParserException : NSException

+(void)raiseWithMessage:(NSString*)message;

@end
