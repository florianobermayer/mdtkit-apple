//
//  MDTestData.m
//  MDTKit
//
//  Created by Florian Obermayer 2016/06/23.
//  Copyright (c) 2016 Secomba GmbH. All rights reserved.
//

#import "MDTestData.h"
#import "MDParserException.h"
#import "MDLinterException.h"
#import "PropertyLoader.h"

static NSString * _testDataBasePath;

@implementation MDTestData

NSString * const TestDataFolderName = @"testdata";

+ (void)setTestDataBasePath:(NSString *)path
{
    _testDataBasePath = path;
}

+ (NSString *) getMDTKitModulePath
{
    return [NSString stringWithFormat:@"%@/%@/%@", [self getModuleWorkingDirectory], @"src", @"mdtkit-parser-cli.js"];
}

+ (NSString *) getModuleWorkingDirectory
{
    NSString * cliBRP = [[NSBundle bundleForClass:[self class]] resourcePath];
    return [cliBRP stringByAppendingPathComponent:[PropertyLoader MDTKitModuleWorkingDir]];
}

+(NSString *) getTestDataBasePath
{
    if (!_testDataBasePath) {
        NSString * folderNamePath = [[NSString alloc] initWithFormat:@"/%@/", TestDataFolderName];
        NSString * bundleResourcePath = [[NSBundle bundleForClass:[self class]] resourcePath];
        NSString * testDataPath = [bundleResourcePath stringByAppendingPathComponent: folderNamePath];
        [self setTestDataBasePath:testDataPath];
    }
    return _testDataBasePath;
}

+ (instancetype)testDataWithClass:(Class)class testName:(NSString *)testName error:(NSError *__autoreleasing *)error
{
    NSString * testClassName = NSStringFromClass(class);
    
    return [[self class] testDataWithTestFileName:testClassName testName:testName error:error];
}

+ (instancetype)testDataWithTestFileName:(NSString *)testFileName testName:(NSString *)testName error:(NSError *__autoreleasing *)error
{
    NSString * filePath = [self findFileRecursiveWithName:testFileName atPath:[self getTestDataBasePath]];
    NSString * parsingResult = [self getParsingResultFromFileAtPath:filePath];
    NSDictionary * dict = [NSJSONSerialization JSONObjectWithData: [parsingResult dataUsingEncoding:NSUTF8StringEncoding] options:0 error:error];
    [self raiseLinterErrorsFromLinter: dict[@"linter"]];
    NSUInteger index = [dict[@"tests"] indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return [obj[@"title"]isEqualToString:testName];
    }];
    
    if(index == NSNotFound && [testName hasPrefix:@"test"]){
        // if not found, remove the "test" prefix from the filename and try again
        [dict[@"tests"] indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            return [obj[@"title"]isEqualToString:[testName substringFromIndex:4]];
        }];
        if(index == NSNotFound){
            [MDParserException raiseWithMessage: [NSString stringWithFormat: @"Cannot find '%@' in file '%@'!", testName, [filePath lastPathComponent]]];
        }
    }
    return [[MDTestData alloc] initWithTestTable:
            [self extractTestTableFromTestCase: dict[@"tests"][index]] preConstants:
            [self extractPrePostConstantsFromTestCase: dict[@"tests"][index] fromPre: YES] postConstants:
            [self extractPrePostConstantsFromTestCase: dict[@"tests"][index] fromPre: NO]];
    
}


+(NSString *) getParsingResultFromFileAtPath:(NSString *) path
{
    // TODO:CONTINUE HERE
    NSString * mdtkitPath = [self getMDTKitModulePath];
    NSString * testfilePath = path;
    
    NSString * output;
    NSString * errorStr;
    
    int exitCode = [self getExitCodeFromProcessName:[PropertyLoader NodePath] arguments: @[mdtkitPath, testfilePath] outputString:&output errorString:&errorStr workingDirectory: [PropertyLoader MDTKitModuleWorkingDir]];
    
    if(exitCode != 0)
    {
        [self tryInitNodeModule];
        
        exitCode = [self getExitCodeFromProcessName:[PropertyLoader NodePath] arguments: @[mdtkitPath, testfilePath] outputString:&output errorString:&errorStr workingDirectory: [self getModuleWorkingDirectory]];
        if(exitCode != 0)
        {
            [MDParserException raiseWithMessage: [NSString stringWithFormat:@"Calling node module 'mdtkit-parser' failed. ExitCode: %d (%@)", exitCode, errorStr]];
        }
    }
    return output;
}

+(void) tryInitNodeModule
{
    NSLog(@"Initializing node module mdtkit-parser (updating node modules)...");
    NSString * output;
    NSString * errorStr;

    [self getExitCodeFromProcessName:[PropertyLoader NPMPath] arguments:@[@"update"] outputString:&output errorString:&errorStr workingDirectory:[self getModuleWorkingDirectory]];
}

+(int) getExitCodeFromProcessName:(NSString*)processName arguments:(NSArray *)args outputString:(NSString **)outputStr errorString:(NSString **)errorStr workingDirectory:(NSString *) workingDir
{
    NSPipe *outPipe;
    NSFileHandle *outFile;
    NSPipe *errPipe;
    NSFileHandle *errFile = errPipe.fileHandleForReading;
    
    NSMutableData *outData;
    NSMutableData *errData;

    NSTask *task = [[NSTask alloc] init];
    task.launchPath = processName;
    task.arguments = args;
    
    if(outputStr){
        outPipe = [NSPipe pipe];
        outFile = outPipe.fileHandleForReading;
        task.standardOutput = outPipe;
        outData = [NSMutableData dataWithCapacity:512];
    }
    if(errorStr){
        errPipe = [NSPipe pipe];
        errFile = errPipe.fileHandleForReading;
        task.standardError = errPipe;
        errData = [NSMutableData dataWithCapacity:512];
    }
    
    [task launch];
    
    while ([task isRunning]) {
        [outData appendData: [outFile readDataToEndOfFile]];
        [errData appendData: [errFile readDataToEndOfFile]];
    }
    
    [task waitUntilExit];
    
    if(outputStr)
    {
        *outputStr = [[NSString alloc] initWithData: outData encoding: NSUTF8StringEncoding];
        [outFile closeFile];
    }

    if(errorStr)
    {
        *errorStr = [[NSString alloc] initWithData: errData encoding: NSUTF8StringEncoding];
        [errFile closeFile];
    }
    
    return  [task terminationStatus];
}

+(void)raiseLinterErrorsFromLinter:(NSDictionary *) linter
{
    for (NSDictionary * error in linter[@"errors"]) {
        [MDLinterException raiseWithMessage:error[@"message"] location:error[@"location"] path:error[@"path"]];
    }
    
    for (NSDictionary * stackElem in linter[@"stack"]) {
        [MDLinterStackException raiseWithMessage:stackElem[@"blockName"] location:stackElem[@"location"] path:stackElem[@"path"]];
    }}


+(MDTestTable *) extractTestTableFromTestCase:(NSDictionary *)testCase
{
    NSArray<NSString *> * headings = [testCase valueForKeyPath:@"table.header.value.title"];
    NSArray<NSArray *> * values = [testCase valueForKeyPath:@"table.content.value"];
    NSMutableArray<NSDictionary<NSString *, id> *> * rows = [[NSMutableArray alloc] init];
    
    for(NSArray * row in values) {
        
        NSMutableDictionary * rowDict = [[NSMutableDictionary alloc] init];
        
        for(NSInteger i = 0; i < [headings count]; i++){
            [rowDict setValue: [row objectAtIndex:i] forKey: [headings objectAtIndex:i]];
        }
        [rows addObject: [rowDict copy]];
    }
    
    MDTestTable * table = [[MDTestTable alloc] initWithHeadings:headings rows:[rows copy]];
    return table;
}

+(NSDictionary *) extractPrePostConstantsFromTestCase:(NSDictionary * )testCase fromPre:(BOOL)fromPre
{
    NSMutableDictionary * result = [[NSMutableDictionary alloc] init];
    for (NSDictionary * constant in testCase[fromPre ? @"preConstants" : @"postConstants"]) {
        [result setObject: [constant valueForKeyPath:@"expression.value"] forKey:constant[@"name"]];
    }
    return [result copy] ;
}


+(NSString*)findFileRecursiveWithName:(NSString*)name atPath:(NSString*)path
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSURL *directoryURL = [NSURL fileURLWithPath:path];
    NSArray *keys = [NSArray arrayWithObject:NSURLIsDirectoryKey];
    
    NSDirectoryEnumerator *enumerator = [fileManager
                                         enumeratorAtURL:directoryURL
                                         includingPropertiesForKeys:keys
                                         options:0
                                         errorHandler:^(NSURL *url, NSError *error) {
                                             [MDParserException raiseWithMessage:[NSString stringWithFormat:@"ios specific - can't collect files at url : \"%@\"", [url absoluteString]]];
                                             return YES;
                                         }];
    
    NSString * filePath;
    NSString *fileNameWithExtension = name;
    if(![name containsString:@"."])
    {
        fileNameWithExtension = [NSString stringWithFormat:@"%@.md", name];
    }
    
    for (NSURL *url in enumerator) {
        NSString *stringUrl = [[url absoluteString] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        // if file is found, return path
        if ([stringUrl hasSuffix:fileNameWithExtension]) {
            if (filePath) {
                [MDParserException raiseWithMessage:[NSString stringWithFormat:@"Multiple files found: '%@'", fileNameWithExtension]];
            }
            filePath = [url path];
        }
    }
    
    if (!filePath) {
        [MDParserException raiseWithMessage:[NSString stringWithFormat:@"No file found: '%@'", fileNameWithExtension]];
    }
    return filePath;
}

- (instancetype)initWithTestTable:(MDTestTable *)testTable preConstants:(NSDictionary *)preConstants postConstants:(NSDictionary *)postConstants
{
    self = [super init];
    if(self) {
        _testTable = testTable;
        _preConstants = preConstants;
        _postConstants = postConstants;
    }
    return self;
}


@end
