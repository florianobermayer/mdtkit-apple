//
//  MDTestTableTestCase.h
//  MDTKit
//
//  Created by Florian Obermayer 2016/06/23.
//  Copyright (c) 2016 Secomba GmbH. All rights reserved.
//

#import "MDTestTable.h"
#import <XCTest/XCTest.h>


@interface MDTestCase : XCTestCase

@property (nonatomic, copy) NSString * testDataFolderPath;

- (void) runOnEachRow :(void (^)(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post)) run;

- (void) runOnEachRowWithTestName:(NSString *)testName testFileName:(NSString *)testFileName withBlock:(void (^)(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post)) run;

- (void) runOnEachRowWithTestName:(NSString *)testName testClass:(Class)testClass withBlock:(void (^)(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post)) run;

- (void)runOnEachRowWithTestName:(NSString *)testName withBlock:(void (^)(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post))run;

-(void) dataForTest:(NSString *)testName runOnEachRow:(void (^)(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post)) run __attribute__ ((deprecated));

-(void) dataForTest:(NSString *)testName class:(Class)testClass runOnEachRow:(void (^)(NSDictionary *row, NSInteger index, NSDictionary *pre, NSDictionary *post)) run __attribute__ ((deprecated));

-(void) AssertExpectExceptionOfType:(Class)exceptionClass withMessageStartingWith:(NSString *)messagePrefix inBlock:(void (^)()) run;


@end
