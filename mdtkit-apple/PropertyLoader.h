//
//  PropertyLoader.h
//  mdtkit-apple
//
//  Created by Florian Obermayer on 23.06.16.
//  Copyright © 2016 Secomba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PropertyLoader : NSObject
extern NSString * const ConfigFileName;
extern NSString * const NODE_PROPERTY_KEY;
extern NSString * const NPM_PROPERTY_KEY;
extern NSString * const MDTKIT_PROPERTY_KEY;

+ (NSString *) NodePath;
+ (NSString *) NPMPath;
+ (NSString *) MDTKitModuleWorkingDir;
@end
