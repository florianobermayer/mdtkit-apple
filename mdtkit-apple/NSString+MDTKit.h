//
//  NSString+MDTKit.h
//  MDTKit
//
//  Created by Florian Obermayer 2016/06/23.
//  Copyright (c) 2016 Secomba GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MDTKit)

- (NSString *)trim;
- (NSString *)removeAllWhitespace;
- (NSString *)removeAllNewLines;
- (NSString *)stringByRemovingString:(NSString *)string;
- (NSArray *)componentsBySplittingStringAtFirstOccurrenceOfString:(NSString *)string;

@end