//
//  mdtkit-apple.h
//  MDTKit
//
//  Created by Florian Obermayer 2016/06/23.
//  Copyright (c) 2016 Secomba GmbH. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for mdtkit-apple.
FOUNDATION_EXPORT double mdtkit_appleVersionNumber;

//! Project version string for mdtkit-apple.
FOUNDATION_EXPORT const unsigned char mdtkit_appleVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <mdtkit_apple/PublicHeader.h>


