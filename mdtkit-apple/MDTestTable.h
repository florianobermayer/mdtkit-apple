//
//  MDTestTable.h
//  MDTKit
//
//  Created by Florian Obermayer 2016/06/23.
//  Copyright (c) 2016 Secomba GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDTestTable : NSObject

@property (copy, nonatomic) NSArray *headings;
@property (copy, nonatomic) NSArray *rows;

- (instancetype)initWithHeadings:(NSArray *)headings rows:(NSArray *)rows;

- (NSArray *)allRows;

@end
