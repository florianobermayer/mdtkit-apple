Codeblock "codeblock"
 = Codeblock_CODEQUOTES_ENTER code:ValidCode Codeblock_CODEQUOTES_LEAVE
{
  return code;
}

Codeblock_CODEQUOTES_ENTER
 = c:CODEQUOTES
 {
   check("Codeblock_CODEQUOTES_ENTER", location);
   return c;
 }

Codeblock_CODEQUOTES_LEAVE
 = c:CODEQUOTES
 {
   check("Codeblock_CODEQUOTES_LEAVE", location);
   return c;
 }


ValidCode
 = SPACING lines:ValidCodeLine*
 {
   const result = {
     "constants" : [],
     "preConstants" : [],
     "postConstants" : [],
     "comments": [],
     "imports": []
   }

   _.each(flattenArray(lines), elem => {

     if(!_.isObject(elem)){
       return;
     }

     if(elem["const_type"] !== undefined) { // pre or post constants

       if(elem["const_type"] === "pre"){
         delete elem["const_type"];
         result["preConstants"].push(elem);
       }else{
         delete elem["const_type"];
         result["postConstants"].push(elem);
       }

     }else if(elem["name"] !== undefined) { // constants: "name" is also in pre/post but they where already handled
       result["constants"].push(elem);

     }else if(_.has(elem, "type")){
       const t = elem["type"];
       if(t === Type.IMPORT){
         result["imports"].push(elem);
       }else if(t === Type.COMMENT){
         result["comments"].push(elem);
       }
    }
   });

   return result;
 }

ValidCodeLine
 = _ ConstDefinition _ SingleComment?
 / _ PrePostDefinition _ SingleComment?
 / _ CodeComment _
 /_ ImportStatement _ SingleComment?
 / _ SPACING _

CodeComment
 = SingleComment
 / MultiComment


ImportStatement_ENTER
 = i:"import"
 {
   check("ImportStatement_ENTER", location);
   return i;
 }

ImportStatement_LEAVE
 = v:">"
 {
   check("ImportStatement_LEAVE", location);
   return v;
 }

ImportStatement
 = ImportStatement_ENTER __ "<" text:[^>]* ImportStatement_LEAVE
 {
   return buildValue(text.join(""), Type.IMPORT, location, path);
 }

 // see http://stackoverflow.com/questions/26556586/peg-js-how-to-parse-c-style-comments
SingleComment


 = '//' (!NEWLINE .)*
 {
   return buildValue(text, Type.COMMENT, location, path);
 }

MultiComment
 = "/*" (!"*/" .)* "*/"
 {
   return buildValue(text, Type.COMMENT, location, path);
 }
/*
  A valid literal + support for multiline values like JSON
*/
ValidCodeblockLiteral
 = ValidSingleLineLiteral
  / MultilineStringLiteral
  / JSONLiteral
  / UntypedLiteral

ConstDefinition_CONST_ENTER
 = c:CONST
 {
   check("ConstDefinition_CONST_ENTER", location);
   return c;
 }

ConstDefinition_SEMICOLON_LEAVE
  = s:SEMICOLON
  {
    check("ConstDefinition_SEMICOLON_LEAVE", location);
    return s;
  }

ConstDefinition
 = ConstDefinition_CONST_ENTER __ typename:ValidTypeName _ EQUALS _ expression:ValidCodeblockLiteral _ ConstDefinition_SEMICOLON_LEAVE
 {
   return{
       "name": typename,
       "expression": expression
   };
 }

PrePostDefinition_PreOrPost_ENTER
 = p:PreOrPost
 {
   check("PrePostDefinition_PreOrPost_ENTER", location);
   return p;
 }

PrePostDefinition_SEMICOLON_LEAVE
 = s:SEMICOLON
 {
   check("PrePostDefinition_SEMICOLON_LEAVE", location);
   return s;
 }

PrePostDefinition
 = const_type:PrePostDefinition_PreOrPost_ENTER __ typename:ValidTypeName COLON validType:ValidType _ EQUALS _ expression:ValidCodeblockLiteral _ PrePostDefinition_SEMICOLON_LEAVE
 {
   return{
     "const_type": const_type,
     "name": typename,
     "validType": validType,
     "expression": expression
   };
 }

PreOrPost
 = PRE
 / POST
