'use strict';

const MDTKitParser = require("./mdtkit-parser");
const parser = MDTKitParser.instance;
const _ = require("lodash");
const path = require("path");

const args = _.drop(process.argv, 2);

if(args.length == 1){
    const result = parser.parse(args[0]);
    console.log(JSON.stringify(result));
}