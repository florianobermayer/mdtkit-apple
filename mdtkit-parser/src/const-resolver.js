'use strict';

const _ = require("lodash");
const Type = require("./literal-type");

/**
 * @callback ConstResolver~replaceCallback
 * @param {string} current
 * @param {string} previous
 * @param {Type} type
 * @param {boolean} finalReplacement
 */
    
class ConstResolver{

    constructor(/**Object=*/constantsDictionary){
        this.dict = [];
        this.heap = [];

        if(constantsDictionary !== undefined){
            this.addRange(constantsDictionary);
        }
    }

    /**
     *
     * @param {ConstResolver~replaceCallback} replaceAction
     *
     */
    replace(replaceAction){
        /*
        if(_.size(this.heap) > 0){
            throw new Error("Some entries could not be resolved." + this.heapToString());
        }*/

        _.each(this.dict, elem => {
            const value = elem.entries;
            const key = elem.key;
            const type = elem.type;

            _.each(value, linkedList => {

                if(_.size(linkedList) < 2){
                    replaceAction(_.first(linkedList), key, type, true);
                    return;
                }
                for( let i = linkedList.length-1; i > 0; i--){
                    const current = linkedList[i];
                    const prev = linkedList[i-1];

                    // replace all elements of this list backwards with each other
                    replaceAction(current, prev, type, false);
                }
                // replace the first element of this list with the key of the map
                replaceAction(_.first(linkedList), key, type, true);
            });
        });
    }

    addRange(constantsDict){
        _.each(constantsDict, (value, key) => {
            this.add(key, value.value, value.type);
        });
    }

    /**
     *
     * @param {String} constName
     * @param {String|Number} value
     * @param {Type} type
     */
    add(constName, value, type){
        if(!ConstResolver.isConstName(value)) {

            this.insertValue(constName, value, type);
        }
        else {
            this.insertConstName(constName, ("" + value).substring(1)); // remove "$" from const name hidden in value
        }

        this.tryChainFromHeap();
    }

    static isConstName(value) {
        if(!_.isString(value)){
            return false;
        }
        return value.startsWith("$");

    }

    /**
     *
     * @param {String} constName
     * @param {String} constNameInValue
     */
    insertConstName(constName, constNameInValue) {
        let found = false;

        _.each(_.filter(this.heap, innerList => _.last(innerList) === constNameInValue), list => {
            list.push(constNameInValue);
            found = true;
        });

        _.each(_.filter(this.heap, innerList => _.first(innerList) === constName), list => {
            list.unshift(constNameInValue);
            found = true;
        });

        // if its not, put [constNameInValue <- constName] in Heap as LinkedList
        if (!found) {
            let ll = [];
            ll.push(constNameInValue);
            ll.push(constName);
            this.heap.push(ll);
        }
    }

    tryChainFromHeap() {

    // finally, go through the dict itself and try to chain but only at the end. on success, remove chainable list from heap.
        const linkedListsToRemoveFromHeap = [];

        _.each(this.dict, elem => {
            const dictList = elem.entries;
            _.each(dictList, linkedList => {
                _.each(_.filter(this.heap, hll => _.last(linkedList) === _.first(hll)), heapLinkedList => {
                    linkedListsToRemoveFromHeap.push(heapLinkedList);

                    for(let i = 1; i< _.size(heapLinkedList); i++){
                        linkedList.push(heapLinkedList[i]);
                    }
                });

                _.each(linkedListsToRemoveFromHeap, llToRemove => _.pull(this.heap, llToRemove));
                _.remove(linkedListsToRemoveFromHeap, elem => true);
            });
        });
    }

    /**
     *
     * @param {String} constName
     * @param value
     * @param {Type} type
     */
    insertValue(constName, value, type) {

        if(_.find(this.dict, ['key', value]) === undefined){

            this.dict.push({
                key: value,
                entries: [],
                type: type
            });
        }

        _.find(this.dict, ['key', value]).entries.push([constName]);

    }

    toString(){
        return "Dict:\n" + this.dictToString() + ",\n\nHeap:\n" + this.heapToString() + "\n";
    }


    dictToString() {
        let result = "{\n";

        _.each(this.dict, elem => {
            const value = elem.entries;
            const key = _.isObject(elem.key)? JSON.stringify(elem.key, null, 2):elem.key;
            result += "  " + elem.type.toString() + " - [" + key + "]:{\n    ";
            result += _.join(_.map(value, ConstResolver.linkedListToString), ",\n    ");
            result += "\n  },\n";
        });

        if(result.endsWith(",\n")) {
            result = result.substring(0, result.length - 1);
        }
        result += "\n}";
        return result;
    }

    heapToString() {
        const heapContent = _.join(_.map(this.heap, ConstResolver.linkedListToString), ",\n ") + "";
        return "{" + (heapContent === ""?"":"\n") + heapContent + (heapContent === ""?"":"\n") + "}";
    }

    static linkedListToString(linkedList){
        return "[" + _.join(linkedList, " <- ") + "]";
    }
}

module.exports = ConstResolver;