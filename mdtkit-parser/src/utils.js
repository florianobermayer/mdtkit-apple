/**
 * Created by Florian on 10.11.2015.
 */
'use strict';

const _ = require("lodash");
const Type = require("./literal-type");
const path = require("path");
const fs = require("fs");

class Utils {

    static parseToList(arg, idx){

        const result = [];
        for(var i = 0; i< arg.length; i++){
            result.push(arg[i][idx]);
        }
        return result;
    }

    static flattenArray(arr){
        return arr.reduce(function (flat, toFlatten) {
            return flat.concat(Array.isArray(toFlatten) ? Utils.flattenArray(toFlatten) : toFlatten);
        }, []);
    }

    static checkDuplicateTestCases(testCases, errorManager){
        const duplicateTestCases = _.filter(testCases, (testCase, i, array) =>  _.isObject(_.find(_.drop(array, i + 1), {title: testCase.title})));

        _.each(duplicateTestCases, testCase => {
            errorManager.raise("Duplicate declaration of TestCase '" + testCase.title + "'", testCase.location, testCase.path);
        });
    }

    /**
     *
     * @param constants
     * @param constantSuffix
     * @param {ErrorManager} errorManager
     */
    static checkDuplicateConstants(constants, constantSuffix, errorManager){
        const duplicateConstants = _.filter(constants, (constant, i, array) =>  _.isObject(_.find(_.drop(array, i + 1), {name: constant.name})));
        _.each(duplicateConstants, dupe => {
            errorManager.raise("Duplicate declaration of " + constantSuffix + "constant '" + dupe.name + "'", dupe.expression.location, dupe.expression.path);
        });
    }

    static validateTable(table){
        const result = {
            valid: false,
            error: null,
            table: null
        };

        const headerNames = _.map(table.header, header => header.value.title);
        const duplicateHeaders = _.filter(headerNames, (x, i, array) => _.includes(array, x, i + 1));
        
        if(duplicateHeaders.length !== 0){
            result.error = "Duplicate header: '" + duplicateHeaders.join(", ") + "'";
            return result;
        }

        const headerLength = table.header.length;
        if(table.formatRow.length != headerLength){
            result.error = "header count := " + headerLength + " != " + table.formatRow.length + " =: format row count";
            return result;
        }
        
        delete table.formatRow;

        for(var i = 0; i< table.content.length; i++){
            var row = table.content[i];
            if(row.length != headerLength){
                result.error = "header count := " + headerLength + " != " + row.length + " =:  row[" +i +"] count";
                return result;
            }

            _.each(row, (cell, i) => {
                cell.validType = table.header[i].value.type;  // put the table type into the content cell object
            });
        }

        result.table = table;
        result.valid = true;
        return result;
    }

    static parseGlobalContentRegion(list){

        const result = {
            "globalComments" : [],
            "globalCodeblocks" : [],
            "globalTable" : null
        };

        if(!_.isArray(list)){
            return result;
        }

        _.each(list, (elem) => {

            if(_.isString(elem)){
                return;
            }

            if(elem.codeblock !== undefined){
                result.globalCodeblocks.push(elem.codeblock);
            }
            if(elem.comment !== undefined){
                result.globalComments.push(elem.comment);
            }
            if(elem.table !== undefined){
                result.globalTable = elem.table;
            }
        });

        return result;
    }

    static buildValue(value, type, location, path){
        if( _.isFunction(value)){
            value = value();
        }

        if( _.isFunction(type)){
            type = type();
        }

        if( _.isFunction(location)){
            location = location();
        }

        if( _.isFunction(path)){
            path = path();
        }

        return {
            value,
            type,
            location,
            path};
    }

    /**
     *
     * @param {String} testTitle
     * @param {Array<Type>} requiredTypes
     * @returns {{constants: Array, tableValues: Array, prePostConstants: Array}}
     */
    static getValuesOfType(parserResult, testTitle, requiredTypes){

        const result = {constants:[], tableValues:[], prePostConstants:[]};
        if(requiredTypes instanceof  Type){
            requiredTypes = [requiredTypes];
        }

        //for global constants:
        _.each(parserResult.globalContent.globalCodeblocks, codeblock =>{
            // find all consts, ...
            _.each(_.filter(codeblock.constants, constant => _.some(requiredTypes, constant.expression.type)), elem => {
                result.constants.push(elem);
            });

            // ... pres, ...
            _.each(_.filter(codeblock.preConstants, constant => _.some(requiredTypes, constant.expression.type)), elem => {
                result.prePostConstants.push(elem);
            });

            // ... posts, ...
            _.each(_.filter(codeblock.postConstants, constant => _.some(requiredTypes, constant.expression.type)), elem => {
                result.prePostConstants.push(elem);
            });
        });

        // for test specific constants:
        const testCase = _.find(parserResult.testCases, testCase => testCase.title === testTitle);
        _.each(testCase.codeblocks, codeblock =>{

            // find all consts, ...
            _.each(_.filter(codeblock.constants, constant => _.some(requiredTypes, constant.expression.type)), elem => {
                result.constants.push(elem);
            });

            // ... pres, ...
            _.each(_.filter(codeblock.preConstants, constant => _.some(requiredTypes, constant.expression.type)), elem => {
                result.prePostConstants.push(elem);
            });

            // ... posts, ...
            _.each(_.filter(codeblock.postConstants, constant => _.some(requiredTypes, constant.expression.type)), elem => {
                result.prePostConstants.push(elem);
            });
        });

        // ... and table values.
        _.each(testCase.table.content, row =>{
            _.each(_.filter(row, cell => _.some(requiredTypes, cell.type)), elem =>{
                result.tableValues.push(elem);
            });
        });

        return result;
    }

    static replaceAll(inputStr, searchStr, replacement) {
        return inputStr.split(searchStr).join(replacement);
    }


    /**
     * @param {string} validType
     * @param {Type} type
     */
    static isCorrectType(validType, type) {
        if(type === Type.NULL){
            return true;
        }
        return type.validTypeName === validType;
    }
    static ensureDirectoryExistence(filePath) {
        const dirName = path.dirname(filePath);
        if (Utils.directoryExists(dirName)) {
            return true;
        }
        Utils.ensureDirectoryExistence(dirName);
        fs.mkdirSync(dirName);
    }

    static directoryExists(path) {
        try {
            return fs.statSync(path).isDirectory();
        }
        catch (err) {
            return false;
        }
    }

    static createDate(parsedDate){
        return new Date(parsedDate).toISOString();
    }

    /**
     *
     * @param {String} filepath
     * @returns {boolean}
     */
    static fileExists(filepath) {
        try{
            fs.statSync(filepath);
        }catch(err){
            if(err.code == 'ENOENT') return false;
        }
        return true;
    }

    static arrUnique(arr) {
        const cleaned = [];
            _.each(arr, itm => {
                let unique = true;
                _.each( cleaned, itm2 => {
                    if (_.isEqual(itm, itm2)){
                        unique = false;
                    }
                });
                if (unique) {
                    cleaned.push(itm);
                }
            });
        return cleaned;
    }
}


module.exports = Utils;


