NoMarkdownTestMethodFoundTest (cannot be found because of this parenthetical text)
----------------------------------------------------------------------------------

This this test test the behavior if the defined test method cannot be found as heading in the markdown file.

Therefore, the test implementation **must not** look for the whole test title but only for something non existing (e.g. ***NoMarkdownTestMethodFoundTest***).

It expects a `MDParserException("no test matching 'NoMarkdownTestMethodFoundTest' found")` Exception.

| var:number |
|:----------:|
|     0      |
