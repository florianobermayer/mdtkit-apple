/**
 * Created by Florian on 09.02.2016.
 */
'use strict';

const MDTKitParser = require("../src/mdtkit-parser");
const parser = MDTKitParser.instance;
const _ = require("lodash");

module.exports = {
    smokeTest
};

function smokeTest(test) {
    const parsingResult = parser.parse("test/testdata/MDTKitTests/MDTKitTests.md");
    test.ok(!_.isNull(parsingResult));
    test.equal(JSON.stringify(parsingResult.linter.errors), JSON.stringify([]));
    test.equal(JSON.stringify(parsingResult.linter.stack), JSON.stringify([]));

    console.log(JSON.stringify(parsingResult.linter, null, 2));

    test.done();
}



