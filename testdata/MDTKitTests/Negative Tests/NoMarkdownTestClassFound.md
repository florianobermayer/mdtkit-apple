NoMarkdownTestClassFoundTest
----------------------------

This test tests the behavior if no test class is found at all.

To implement this test, create a class `MyNotInMarkdownTestFolderFindableClass` and let the test run upon this class. **Do not** create a matching markdown file (e.g. 'MyNotInMarkdownTestFolderFindableClass.md').

It expects a `MDParserException("no file found: MyNotInMarkdownTestFolderFindableClass.md")` Exception.
